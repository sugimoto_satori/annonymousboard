package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Report;
import com.example.demo.service.ReportService;

@Controller
public class ForumController {

	// インスタンスの作成
	@Autowired
	ReportService reportService;

	// 投稿内容表示画面（掲示部分）
	@GetMapping
	public ModelAndView top() {

		// Modelと同様
		ModelAndView mav = new ModelAndView();

		// 投稿を全件取得
		List<Report> contentData = reportService.findAllReport();

		// 画面遷移先を指定
		mav.setViewName("/top");

		// 取得した投稿データオブジェクトを保管
		mav.addObject("contents", contentData);

		// 保管したmavを返す。
		return mav;

	}

	// 新規投稿画面への遷移
	@GetMapping("/new")
	public ModelAndView newContent() {

		// mavの準備
		ModelAndView mav = new ModelAndView();

		// entityの準備
		Report report = new Report();

		// 画面遷移先の指定
		mav.setViewName("/new");

		// entityの保管
		mav.addObject("formModel", report);

		// 保管したmavを返す。
		return mav;

	}

	//投稿取得処理
	@GetMapping("/edit/{id}")
	public ModelAndView editContent(@PathVariable Integer id) {

		ModelAndView mav = new ModelAndView();

		//投稿をDBから取得
		Report report = reportService.editReport(id);

		//編集する投稿をセット
		mav.addObject("formModel", report);

		//画面遷移先を指定
		mav.setViewName("/edit");
		return mav;
	}

	// 投稿処理
	@PostMapping("/add")
	public ModelAndView addContent(@ModelAttribute("formModel") Report report) {

		// 投稿をDBに送信
		reportService.saveReport(report);

		// topへリダイレクト
		return new ModelAndView("redirect:./");
	}

	@DeleteMapping("/delete/{id}")
	public ModelAndView deleteContent(@PathVariable Integer id) {

		//削除命令をDBに送信
		reportService.deleteReport(id);

		//topへリダイレクト
		return new ModelAndView("redirect:/");
	}

	// 編集処理
	@PutMapping("/update/{id}")
	public ModelAndView updateContent (@PathVariable Integer id, @ModelAttribute("formModel") Report report) {

		//idを装填？
		report.setId(id);

		//投稿の更新
		reportService.saveReport(report);

		// rootへリダイレクト
		return new ModelAndView("redirect:/");

	}

}